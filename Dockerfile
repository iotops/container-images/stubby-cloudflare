FROM ubuntu:18.04
LABEL manteiner="Francisco Bobadilla"
LABEL manteiner_email="frankmb@protonmail.com"
LABEL release_date="12 feb 2019"
RUN apt update -y; \
    apt upgrade -y; \
    apt install -y stubby dnsutils; \
    apt autoremove -y; \
    apt autoclean -y
ADD ./stubby.yml /etc/stubby/stubby.yml
EXPOSE 54
CMD ["/usr/bin/stubby"]
